<?php
        if ( !defined( 'ABSPATH' ) ) { exit; }

        if ( ! defined( 'LOCALPRESS_VERSION' ) ) {
            define( 'LOCALPRESS_VERSION', '1.5' );
        }

        // Required Functions Start

        require get_stylesheet_directory() . '/inc/admin.php';  // Admin Panel by CSF
        require get_stylesheet_directory() . '/inc/template-functions.php';


        // Auto Updater Function

        require 'lib/updater/plugin-update-checker.php';
        $WPLocalPressUpdater = Puc_v4_Factory::buildUpdateChecker(
        'https://api.themespell.com/updater/?action=get_metadata&slug=wp-localpress',
        __FILE__,
        'wp-localpress'
        );


