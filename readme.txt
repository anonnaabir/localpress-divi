=== LocalPress ===

Requires at least: 4.5
Tested up to: 5.7
Requires PHP: 7.0
Stable tag: 1.5
License: GNU General Public License v2 or later
License URI: LICENSE

The Best Local Business WordPress Theme That Ever Been Made

== Description ==

No matter if you are running a small local startup, business, or an enterprise, you will always be able to build the best website for your local business using the LocalPress local business WordPress theme.

Quality comes with high price, therefore choose the best one. LocalPress theme is entirely focused on Local Business. You handle the business and leave your website to LocalPress local business WordPress theme.



== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload Theme and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.



== Changelog ==

= 1.0 - August 18, 2020 =
* Initial release


= 1.1 - November 23, 2020 =
* Added 30+ Demos


= 1.2 - February 05, 2021 =
* Added 50+ Demos
* Additional Bug Fixed

= 1.3 - April 06, 2021 =
* One Click Demo Import Too Many Redirect Fixed


= 1.4 - April 15, 2021 =
* Fixed Old Demos Issues
* Additional Bug Fixed


= 1.4.1 - May 05, 2021 =
* Added 'Grocery Shop' Demo
* Added 'Furniture Repair Shop' Demo


= 1.4.2 - June 02, 2021 =
* Added 'Carpet Cleaning' Demo
* Added 'Vaccum Cleaning' Demo
* Updated Notification From WordPress admin
* Compatible and Tested with WordPress 5.7.2


= 1.4.3 - July 07, 2021 =
* Added 'Holiday Decoration Service' Demo
* Added 'Wealth Management Service' Demo
* Added 'Kindergarten' Demo
* Added 'Flooring Service' Demo
* Added 'Dog Fencing Service' Demo
* Additional Bug Fixed


= 1.5 - September 22, 2021 =
* Rebuilt Theme With Custom Framework
* New Dedicated Option Panel
* New Required Plugin Installer
* Added Call Popup Feature
* Added White Label Feature
* Added Limit Access Feature
* Added Limit Access Feature
* Added 'Home Security System' Demo
* Added 'Financial Planning' Demo
* Added 'HVAC Service' Demo
* Added 'Pest Control Service' Demo
* Added 'Tax Service' Demo
* Added 'Local SEO Service' Demo
* Additional Bug Fixed
* Localize Feature Updated
* Performance Update
