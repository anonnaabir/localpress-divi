jQuery(document).ready(function(){
    var action_type = localpress_controls.call_popup_action_type;
    var action_link = localpress_controls.call_popup_action_link;
    var action_phone = localpress_controls.call_popup_action_phone;
    var phone_number = localpress_controls.call_popup_text;
    var icon = localpress_controls.call_popup_icon;
    var vertical_position = localpress_controls.call_popup_vertical_position;
    var horizontal_position = localpress_controls.call_popup_horizontal_position;
    var animation_type = localpress_controls.call_popup_animation_type;
    var animation_repeat = localpress_controls.call_popup_animation_repeat;
    var animation_speed = localpress_controls.call_popup_animation_speed;

    if (action_type == 'action_url') {
        var call_popup_action = action_link;
    }

    else if (action_type == 'action_phone') {
        var call_popup_action = 'tel:'+ action_phone;
    }

    jQuery(
        '<a href="'+call_popup_action+'"><div class="call-popup animate__animated animate__'+animation_type+' '+animation_repeat+' '+animation_speed+'"><i class="call_popup_icon '+ icon +'"></i><p>'+ phone_number + '</p></div></a>'
        ).appendTo('body');

        if (vertical_position == 'bottom') {
            jQuery(".call-popup").css("top", "85%");
        }
        
        if (vertical_position == 'top') {
            jQuery(".call-popup").css("bottom", "85%");
        }

        if (vertical_position == 'center') {
            jQuery(".call-popup").css("bottom", "45%");
        }

        if (horizontal_position == 'left') {
            jQuery(".call-popup").css("left", "10%");
        }

        if (horizontal_position == 'right') {
            jQuery(".call-popup").css("right", "10%");
        }

        if (horizontal_position == 'center') {
            jQuery(".call-popup").css("left", "45%");
        }

    });