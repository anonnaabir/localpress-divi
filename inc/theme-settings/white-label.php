<?php

if (! class_exists ('LocalPress_White_Label') ) {

    Class LocalPress_White_Label {

        public function __construct(){
            $lp_options = get_option( 'localpress_options' );
            
            if ( $lp_options['wp_localpress_white_label'] == true) {
                add_filter( 'wp_prepare_themes_for_js', array( $this, 'localpress_data_update' ));
                add_action( 'admin_menu', array( $this, 'localpress_hide_menus'), 999 );
                if ($lp_options['white_label_hide_plugins'] == true) {
                    add_action('pre_current_active_plugins', array( $this, 'localpress_hide_plugins'));
                }
            }
        }


        public function localpress_data_update($themes) {
            $lp_options = get_option( 'localpress_options' );
            
            // Theme Name
            $themes['wp-localpress']['name']= $lp_options['white_label_theme_name'];
            
            // Theme Description
			$themes['wp-localpress']['description']= $lp_options['white_label_theme_description'];

            // Theme Version 
            $themes['wp-localpress']['version']= $lp_options['white_label_theme_version'];

            // Theme Author
			$themes['wp-localpress']['author']= $lp_options['white_label_theme_author'];

            // Theme Author & URL
			$themes['wp-localpress']['authorAndUri']= '<a href="'.$lp_options['white_label_theme_author_url'].'">'.$lp_options['white_label_theme_author'].'</a>';
			
            // Theme Screenshot
			$themes['wp-localpress']['screenshot'][0] = $lp_options['white_label_theme_screenshot']['url'];

            // Theme Tags
			$themes['wp-localpress']['tags']= $lp_options['white_label_theme_tags'];

			return $themes;
        }


        public function localpress_hide_plugins() {
            global $wp_list_table;
            $selected_plugins = array(
                'one-click-demo-import/one-click-demo-import.php'
            );

            $plugins = $wp_list_table->items;
            
            foreach ($plugins as $key => $val) {
              if (in_array($key,$selected_plugins)) {
                unset($wp_list_table->items[$key]);
              }
            }
          
        }


        public function localpress_hide_menus() {
            global $submenu;
            $lp_options = get_option( 'localpress_options' );

            if ($lp_options['white_label_hide_demo_import'] == true) {
                remove_submenu_page('themes.php', 'one-click-demo-import');
                remove_submenu_page('themes.php', 'wp-localpress-suggested-plugins');
            }

            if ($lp_options['limit_access_hide_wordpress_settings'] == true) {
                // Hide WordPress Settings
                remove_menu_page( 'options-general.php' ); 
            }

            if ($lp_options['limit_access_hide_theme_settings'] == true) {
                // Hide Theme Settings
                remove_menu_page( 'themes.php' ); 
            }

            if ($lp_options['limit_access_hide_plugin_settings'] == true) {
                // Hide Plugin Settings
                remove_menu_page( 'plugins.php' );
            }

            if ($lp_options['limit_access_hide_user_settings'] == true) {
                // Hide User Settings
                remove_menu_page( 'users.php' );
            }

            if ($lp_options['limit_access_hide_tools_settings'] == true) {
                // Hide Tools Settings
                remove_menu_page( 'tools.php' );
            }
    
            // admin.php?page=localpress-options#tab=general-settings
        }


        // public function localpress_remove_menus() {
        //     global $submenu;

        //     // Hide Default Menus
        //     remove_menu_page( 'edit.php?post_type=page' );    //Pages
        //     remove_menu_page( 'edit-comments.php' );          //Comments
        //     // remove_menu_page( 'themes.php' );                 //Appearance
        //     remove_menu_page( 'plugins.php' );                //Plugins
        //     remove_menu_page( 'users.php' );                  //Users
        //     remove_menu_page( 'tools.php' );                  //Tools
        //     remove_menu_page( 'options-general.php' );        //Settings
            
        //     // Hide Elementor Menus
        //     remove_menu_page('elementor');
	    //     remove_menu_page('edit.php?post_type=elementor_library');
            
        //     // admin.php?page=localpress-options#tab=general-settings
        // }
        
 
    }

    $lp_whitelabel_settings = new LocalPress_White_Label();
    
}
