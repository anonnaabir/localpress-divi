<?php

if (! class_exists ('LocalPress_Common_Settings') ) {

    Class LocalPress_Common_Settings {

        public function __construct(){
            $lp_options = get_option( 'localpress_options' );

            if ( $lp_options['wp_localpress_call_popup'] == true) {
                add_action( 'wp_enqueue_scripts', array($this,'localpress_call_popup_assets') );
            }
        }


        // public function localpress_call_popup() {

        // }

        public function localpress_call_popup_assets() {
            $lp_options = get_option( 'localpress_options' );

            wp_enqueue_style( 'fontawesome', get_stylesheet_directory() . '/css/fa.min.css', array(), '5.13.0', 'all');
            wp_enqueue_style( 'animate', get_stylesheet_directory() . '/css/animate.min.css', array(), '4.1.1', 'all');
            wp_enqueue_script( 'main', get_stylesheet_directory() . '/js/main.js', array('jquery'), '1.0', true );
            wp_localize_script( 'main', 'localpress_controls',
            array(
                'call_popup_action_type' => $lp_options['call_popup_action_type'],
                'call_popup_action_link' => $lp_options['call_popup_action_link'],
                'call_popup_action_phone' => $lp_options['call_popup_action_phone'],
                'call_popup_text' => $lp_options['call_popup_number'],
                'call_popup_icon' => $lp_options['call_popup_icon'],
                'call_popup_vertical_position' => $lp_options['call_popup_vertical_position'],
                'call_popup_horizontal_position' => $lp_options['call_popup_horizontal_position'],
                'call_popup_animation_type' => $lp_options['call_popup_animation_type'],
                'call_popup_animation_repeat' => $lp_options['call_popup_animation_repeat'],
                'call_popup_animation_speed' => $lp_options['call_popup_animation_speed'],
            )
    );
        }
 
    }

    $lp_common_settings = new LocalPress_Common_Settings();
    
}
