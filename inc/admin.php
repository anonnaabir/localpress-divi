<?php

// require_once get_stylesheet_directory() .'/lib/csf/codestar-framework.php';
require_once get_theme_file_path() .'/lib/csf/codestar-framework.php';


        if( class_exists( 'CSF' ) ) {

            $prefix = 'localpress_options';
            $translatable_name = __('LocalPress','wp-localpress');
            $lp_options = get_option( 'localpress_options' );
            $menu_display = $lp_options['limit_access_hide_localpress_settings'];

            CSF::createOptions( $prefix, array(
            'framework_title'  => $translatable_name.' Options',
            'framework_class'  => '',
            'menu_title' => $translatable_name.' Options',
            'menu_slug'  => 'localpress-options',
            'menu_icon'  => 'dashicons-admin-home',
            'menu_hidden' => $menu_display,
            'show_bar_menu' => false,
            // 'show_in_customizer'  => true,
            'footer_credit'  => 'Powered By <a href="https://themespell.com/local-business-wordpress-theme/">LocalPress</a>. Made with ♥ by Team <a href="https://themespell.com">Themespell</a>'
            ) );
        
            
            CSF::createSection( $prefix, array(
            'title'  => 'General Settings',
            'fields' => array(

              array(
                'type'    => 'heading',
                'content' => 'General Settings',
              ),

              
              array(
                'id'    => 'wp_localpress_custom_favicon',
                'type'  => 'media',
                'title' => 'Custom Favicon',
                'library' => 'image',
                'preview_width' => 64,
                'preview_height' => 64
              ),
              
            )
            ) );
        

            CSF::createSection( $prefix, array(
            'title'  => 'Call Popup',
            'fields' => array(

              array(
                'type'    => 'heading',
                'content' => 'Call Popup',
              ),

                array(
                    'id'    => 'wp_localpress_call_popup',
                    'type'  => 'switcher',
                    'title' => 'Enable Call Popup',
                  ),

                  array(
                    'type'    => 'subheading',
                    'content' => 'Action Settings',
                    'dependency' => array( 'wp_localpress_call_popup', '==', 'true' ),
                ),


                array(
                    'id'          => 'call_popup_action_type',
                    'type'        => 'select',
                    'title'       => 'Action Type',
                    'placeholder' => 'Select an option',
                    'options'     => array(
                      'action_url'  => 'Go To URL',
                      'action_phone'  => 'Phone Call',
                    ),
                    'dependency' => array( 'wp_localpress_call_popup', '==', 'true' ),                   
                  ),

                  array(
                    'id'    => 'call_popup_action_link',
                    'type'  => 'text',
                    'title' => 'URL',
                    'dependency' => array( 'call_popup_action_type', '==', 'action_url' ),
                  ),                  
                  
                  array(
                    'id'      => 'call_popup_action_phone',
                    'type'    => 'text',
                    'title'   => 'Phone Number',
                    'dependency' => array( 'call_popup_action_type', '==', 'action_phone' ),
                  ),                  

                array(
                    'type'    => 'subheading',
                    'content' => 'Text Settings',
                    'dependency' => array( 'wp_localpress_call_popup', '==', 'true' ),
                ),

                  array(
                    'id'      => 'call_popup_number',
                    'type'    => 'text',
                    'title'   => 'Text',
                    'default' => 'Call Now',
                    'dependency' => array( 'wp_localpress_call_popup', '==', 'true' ),
                  ),

                  array(
                    'id'      => 'call_popup_number_typography',
                    'type'    => 'typography',
                    'title'   => 'Typography',
                    'color'   => false,
                    'subset'  => false,
                    'text_transform'  => false,
                    // 'text_align'  => false,
                    'letter_spacing'  => false,
                    'default' => array(
                    //   'color'       => '#ffbc00',
                      'font-family' => 'Open Sans',
                      'font-size'   => '18',
                      'line-height' => '20',
                      'unit'        => 'px',
                      'type'        => 'google',
                    ),
                    'output' => '.call-popup',
                    'dependency' => array( 'wp_localpress_call_popup', '==', 'true' ),
                  ),
                  
                  array(
                    'id'       => 'call_popup_number_margin',
                    'type'     => 'spacing',
                    'title'    => 'Margin',
                    'output' => '.call-popup p',
                    'output_mode' => 'margin',
                    'default'  => array(
                      'top'    => '10',
                      'right'  => '0',
                      'bottom' => '0',
                      'left'   => '0',
                      'unit'   => 'px',
                    ),
                    'dependency' => array( 'wp_localpress_call_popup', '==', 'true' ),
                  ),
                  

                array(
                    'type'    => 'subheading',
                    'content' => 'Icon Settings',
                    'dependency' => array( 'wp_localpress_call_popup', '==', 'true' ),
                ),

                array(
                    'id'      => 'call_popup_icon',
                    'type'    => 'icon',
                    'title'   => 'Icon',
                    'default' => 'fa fa-heart',
                    'dependency' => array( 'wp_localpress_call_popup', '==', 'true' ),
                  ),


                  array(
                    'id'    => 'call_popup_icon_size',
                    'type'  => 'slider',
                    'title' => 'Icon Size',
                    'min'     => 0,
                    'max'     => 100,
                    'step'    => 1,
                    'unit'    => 'px',
                    'default' => 35,
                    'output' => '.call_popup_icon',
                    'output_mode' => 'font-size',
                    'dependency' => array( 'wp_localpress_call_popup', '==', 'true' ),
                  ),            


                  array(
                    'type'    => 'subheading',
                    'content' => 'Style Settings',
                    'dependency' => array( 'wp_localpress_call_popup', '==', 'true' ),
                ),


                array(
                    'id'          => 'call_popup_vertical_position',
                    'type'        => 'select',
                    'title'       => 'Vertical Position',
                    'placeholder' => 'Select Positiom',
                    'options'     => array(
                      'top'  => 'Top',
                      'center'  => 'Center',
                      'bottom'  => 'Bottom',
                    ),
                    'default'     => 'bottom',
                    'dependency' => array( 'wp_localpress_call_popup', '==', 'true' ),
                  ),


                  array(
                    'id'          => 'call_popup_horizontal_position',
                    'type'        => 'select',
                    'title'       => 'Horizontal Position',
                    'placeholder' => 'Select Positiom',
                    'options'     => array(
                      'left'  => 'Left',
                      'center'  => 'Center',
                      'right'  => 'Right',
                    ),
                    'default'     => 'left',
                    'dependency' => array( 'wp_localpress_call_popup', '==', 'true' ),
                  ),



                array(
                    'id'     => 'call_popup_text_color',
                    'type'   => 'color',
                    'title'  => 'Color',
                    'output' => '.call-popup',
                    'output_mode' => 'color',
                    'dependency' => array( 'wp_localpress_call_popup', '==', 'true' ),
                  ),

                array(
                    'id'     => 'call_popup_background_color',
                    'type'   => 'color',
                    'title'  => 'Background Color',
                    'output' => '.call-popup',
                    'output_mode' => 'background-color',
                    'dependency' => array( 'wp_localpress_call_popup', '==', 'true' ),
                  ),

                
                  array(
                    'id'       => 'call_popup_padding',
                    'type'     => 'spacing',
                    'title'    => 'Margin',
                    'output' => '.call-popup',
                    'output_mode' => 'padding',
                    'default'  => array(
                      'top'    => '10',
                      'right'  => '10',
                      'bottom' => '10',
                      'left'   => '10',
                      'unit'   => 'px',
                    ),
                    'dependency' => array( 'wp_localpress_call_popup', '==', 'true' ),
                  ),

                  array(
                    'id'      => 'call_popup_border',
                    'type'    => 'border',
                    'title'   => 'Border',
                    'output' => '.call-popup',
                    'output_mode' => 'border',
                    'dependency' => array( 'wp_localpress_call_popup', '==', 'true' ),
                  ),

                  array(
                    'id'      => 'call_popup_border_radius',
                    'type'     => 'spacing',
                    'title'   => 'Border Radius',
                    'output' => '.call-popup',
                    'output_mode' => 'border-radius',
                    'default'  => array(
                        'top'    => '10',
                        'right'  => '10',
                        'bottom' => '10',
                        'left'   => '10',
                        'unit'   => 'px',
                      ),
                      'dependency' => array( 'wp_localpress_call_popup', '==', 'true' ),
                  ),


                  array(
                    'type'    => 'subheading',
                    'content' => 'Animation Settings',
                    'dependency' => array( 'wp_localpress_call_popup', '==', 'true' ),
                ),

                
                array(
                    'id'          => 'call_popup_animation_type',
                    'type'        => 'select',
                    'title'       => 'Animation Type',
                    'placeholder' => 'Select animation type',
                    'options'     => array(
                      'bounce'  => 'Bounce',
                      'flash'   =>  'Flash',
                      'pulse'   =>  'Pulse',
                      'rubberBand'   =>  'Rubber Band',
                      'shakeX'   =>  'Shake (Horizantal)',
                      'shakeY'   =>  'Shake (Vertical)',
                      'swing'   =>  'Swing',
                      'tada'   =>  'Tada',
                      'wobble'   =>  'Wobble',
                      'jello'   =>  'Jello',
                      'heartBeat'   =>  'Heart Beat',
                    ),
                    'default'     => 'swing',
                    'dependency' => array( 'wp_localpress_call_popup', '==', 'true' ),
                  ),

                  array(
                    'id'          => 'call_popup_animation_repeat',
                    'type'        => 'select',
                    'title'       => 'Animation Repeat',
                    'placeholder' => 'Select animation repeat',
                    'options'     => array(
                      'animate__repeat-1'  => '01 Time',
                      'animate__repeat-2'   =>  '02 Times',
                      'animate__repeat-3'   =>  '03 Times',
                      'animate__infinite'     => 'Infinite'
                    ),
                    'default'     => 'animate__infinite',
                    'dependency' => array( 'wp_localpress_call_popup', '==', 'true' ),
                  ),

                  array(
                    'id'          => 'call_popup_animation_speed',
                    'type'        => 'select',
                    'title'       => 'Animation Speed',
                    'placeholder' => 'Select animation speed',
                    'options'     => array(
                      'animate__slower'  => 'Very Slow',
                      'animate__slow'   =>  'Slow',
                      ''                =>  'Normal',
                      'animate__fast'   =>  'Fast',
                      'animate__faster' => 'Super Fast'
                    ),
                    'default'     => '',
                    'dependency' => array( 'wp_localpress_call_popup', '==', 'true' ),
                  ),


                
                  
                  
                        
            )
            ) );


            CSF::createSection( $prefix, array(
                'title'  => 'White Labeling',                
                'fields' => array(

                  array(
                    'type'    => 'heading',
                    'content' => 'White Labeling',
                  ),

                  array(
                    'id'    => 'wp_localpress_white_label',
                    'type'  => 'switcher',
                    'title' => 'Enable White Labeling',
                  ),

                  array(
                    'type'    => 'subheading',
                    'content' => 'Theme Settings',
                    'dependency' => array( 'wp_localpress_white_label', '==', 'true' ),
                ),

                array(
                  'id'      => 'white_label_theme_name',
                  'type'    => 'text',
                  'title'   => 'Theme Name',
                  'dependency' => array( 'wp_localpress_white_label', '==', 'true' ),
                ),

                array(
                  'id'      => 'white_label_theme_description',
                  'type'    => 'textarea',
                  'title'   => 'Theme Description',
                  'dependency' => array( 'wp_localpress_white_label', '==', 'true' ),
                ),

                array(
                  'id'      => 'white_label_theme_version',
                  'type'    => 'text',
                  'title'   => 'Theme Version',
                  'dependency' => array( 'wp_localpress_white_label', '==', 'true' ),
                ),

                array(
                  'id'      => 'white_label_theme_author',
                  'type'    => 'text',
                  'title'   => 'Theme Author',
                  'dependency' => array( 'wp_localpress_white_label', '==', 'true' ),
                ),

                array(
                  'id'      => 'white_label_theme_author_url',
                  'type'    => 'text',
                  'title'   => 'Author URL',
                  'dependency' => array( 'wp_localpress_white_label', '==', 'true' ),
                ),

                array(
                  'id'      => 'white_label_theme_screenshot',
                  'type'    => 'media',
                  'title'   => 'Theme Screenshot',
                  'library' => 'image',
                  'dependency' => array( 'wp_localpress_white_label', '==', 'true' ),
                ),


                array(
                  'id'      => 'white_label_theme_tags',
                  'type'    => 'text',
                  'title'   => 'Theme Tags',
                  'dependency' => array( 'wp_localpress_white_label', '==', 'true' ),
                ),


                array(
                  'type'    => 'subheading',
                  'content' => 'Plugin Settings',
                  'dependency' => array( 'wp_localpress_white_label', '==', 'true' ),
              ),
                
              
              array(
                'id'    => 'white_label_hide_plugins',
                'type'  => 'switcher',
                'title' => 'Hide Recommended Plugins',
                'default' => true,
                'dependency' => array( 'wp_localpress_white_label', '==', 'true' ),
              ),

              array(
                'id'    => 'white_label_hide_demo_import',
                'type'  => 'switcher',
                'title' => 'Hide Demo Import',
                'default' => true,
                'dependency' => array( 'wp_localpress_white_label', '==', 'true' ),
              ),

                )
                ) );

                CSF::createSection( $prefix, array(
                  'title'  => 'Limit Access',
                  'fields' => array(

                    array(
                      'type'    => 'heading',
                      'content' => 'Limit Access',
                    ),

                    // A Notice
                  array(
                    'type'    => 'notice',
                    'style'   => 'warning',
                    'content' => 'Please save this URL first. As you can access the settings directly: <strong>'.get_site_url().'/wp-admin/admin.php?page=localpress-options</strong>',
                  ),


                    array(
                      'id'    => 'limit_access_hide_localpress_settings',
                      'type'  => 'switcher',
                      'title' => 'Hide LocalPress Settings',
                    ),

                    array(
                      'id'    => 'limit_access_hide_elementor_settings',
                      'type'  => 'switcher',
                      'title' => 'Hide Elementor Settings',
                    ),

                    array(
                      'id'    => 'limit_access_hide_wordpress_settings',
                      'type'  => 'switcher',
                      'title' => 'Hide WordPress Settings',
                    ),

                    array(
                      'id'    => 'limit_access_hide_theme_settings',
                      'type'  => 'switcher',
                      'title' => 'Hide Theme Settings',
                    ),

                    array(
                      'id'    => 'limit_access_hide_plugin_settings',
                      'type'  => 'switcher',
                      'title' => 'Hide Plugin Settings',
                    ),

                    array(
                      'id'    => 'limit_access_hide_user_settings',
                      'type'  => 'switcher',
                      'title' => 'Hide User Settings',
                    ),

                    array(
                      'id'    => 'limit_access_hide_tools_settings',
                      'type'  => 'switcher',
                      'title' => 'Hide Tools Settings',
                    ),

                  )
                ) );
        
        }
  