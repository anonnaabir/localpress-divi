<?php

			// Added Custom Favicon Support

			function wp_localpress_add_favicon() {
				$lp_options = get_option( 'localpress_options' );
				if (isset($lp_options['wp_localpress_custom_favicon']['url'])) {
					$favicon = $lp_options['wp_localpress_custom_favicon']['url'];
					echo'<link rel="icon" href="'.$favicon.'" type="image/png" sizes="16x16">';
				}
			}
				
			add_action('wp_head', 'wp_localpress_add_favicon');




			// Demo Import Function Start

			function wp_localpress_demo_import() {
				return array(
					array(
						'import_file_name'           => 'Local SEO Service',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://preview.themespell.com/wp-content/uploads/2021/09/localseoservice.WordPress.2021-09-23.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2021/09/localpress_local_seo_service.png',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'wp-localpress' ),
						'preview_url'                => 'https://preview.themespell.com/local-seo-service/',
					),
				);
			}
			add_filter( 'pt-ocdi/import_files', 'wp_localpress_demo_import' );
		
		
		
		
			function wp_localpress_demo_import_page_setup( $default_settings ) {
				$lp_options = get_option( 'localpress_options' );
				
				if (isset($lp_options['white_label_theme_name'])){
					$theme_name = $lp_options['white_label_theme_name'];
				}
				else {
					$theme_name = 'LocalPress';
				}

				$default_settings['parent_slug'] = 'themes.php';
				$default_settings['page_title']  = esc_html__( $theme_name.' Demo Import' , 'wp-localpress' );
				$default_settings['menu_title']  = esc_html__( 'Import '.$theme_name.' Demos' , 'wp-localpress' );
				$default_settings['capability']  = 'import';
				// $default_settings['menu_slug']   = 'pt-one-click-demo-import';
			
				return $default_settings;
			}
			add_filter( 'pt-ocdi/plugin_page_setup', 'wp_localpress_demo_import_page_setup' );
		
		
			function wp_localpress_demo_import_page_title ($plugin_title ) {
				?>
				<h1 class="ocdi__title  dashicons-before  dashicons-upload"><?php $plugin_title  = esc_html_e( 'LocalPress Demo Import', 'wp-localpress' ); ?></h1>
				<?php
			}
			add_filter( 'pt-ocdi/plugin_page_title', 'wp_localpress_demo_import_page_title' );
		
			add_filter( 'pt-ocdi/disable_pt_branding', '__return_true' );

			add_filter( 'pt-ocdi/import_memory_limit', '256M' );
		
		
		
			
		
			// Demo Import Function End






